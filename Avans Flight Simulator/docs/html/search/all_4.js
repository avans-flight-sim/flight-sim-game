var searchData=
[
  ['description_66',['description',['../class_aerofoil.html#a9ff1ca4713be454efaea5f4644214575',1,'Aerofoil']]],
  ['devices_67',['devices',['../class_master_controls.html#a35587f5f8c8e783be1a8ba2cfdb43dab',1,'MasterControls']]],
  ['dimensions_68',['dimensions',['../class_wing.html#a7b5bc537cc6a8bb4119461a85e84d0f3',1,'Wing']]],
  ['disable_69',['Disable',['../class_master_controls.html#ae524c6d9a2bca871cda8be5554607df7',1,'MasterControls.Disable()'],['../struct_master_controls_1_1_airplane_actions.html#a8fb96f0443e8a441d688d66fbf3dd025',1,'MasterControls.AirplaneActions.Disable()'],['../struct_master_controls_1_1_camera_actions.html#ad30d3c3260a1f5fb6040860ccaefda2e',1,'MasterControls.CameraActions.Disable()'],['../struct_master_controls_1_1_user_interface_in_game_actions.html#ad11ac1f21a5b10c970c9770d69b9e82c',1,'MasterControls.UserInterfaceInGameActions.Disable()']]],
  ['discardinput_70',['DiscardInput',['../class_u_i_1_1_screens_1_1_options_screen_manager.html#a08ebf7dd727b1364bec3ec1e6ff96598',1,'UI::Screens::OptionsScreenManager']]],
  ['dispose_71',['Dispose',['../class_master_controls.html#aa4b639047f7dbda571318d9d10715ea8',1,'MasterControls']]],
  ['double_5fformat_72',['DOUBLE_FORMAT',['../class_u_i_1_1_components_1_1_information_container_manager.html#a172d7bd8353cb5799d0d7b47d836bc73',1,'UI::Components::InformationContainerManager']]],
  ['double_5fround_5fdigits_5famount_73',['DOUBLE_ROUND_DIGITS_AMOUNT',['../class_u_i_1_1_components_1_1_information_container_manager.html#afb9cef1ef53657b44599fa4160a65adb',1,'UI::Components::InformationContainerManager']]],
  ['drag_74',['drag',['../class_aerofoil.html#a3e3b804a058e1500a5468294567982a6',1,'Aerofoil']]],
  ['dragcoefficient_75',['dragCoefficient',['../class_wing.html#a631de8ee546c34f23437f9654cd5fb00',1,'Wing.dragCoefficient()'],['../class_wing.html#a1857e9cd6272b90d805a8a1b89b233c1',1,'Wing.DragCoefficient()']]],
  ['dragforce_76',['dragForce',['../class_wing.html#a2edbc7af7dba73f4f2ccc4066d8c1580',1,'Wing.dragForce()'],['../class_wing.html#a69525c83a9da4d62eb1057d29d9c3a4e',1,'Wing.DragForce()']]],
  ['dragmultiplier_77',['dragMultiplier',['../class_wing.html#a928dd07ada6c78d35f187388e11a7176',1,'Wing']]]
];
