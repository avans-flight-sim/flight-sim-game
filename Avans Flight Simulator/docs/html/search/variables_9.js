var searchData=
[
  ['kcriticaldesc_479',['kCriticalDesc',['../class_aerofoil_editor.html#a1a3f30cf3008f61223a4d1de6d6a7bcd',1,'AerofoilEditor']]],
  ['kflatplatemax_480',['kflatPlateMax',['../class_aerofoil_editor.html#a9d0cf90a4454bb18379f8b7bd25e60fa',1,'AerofoilEditor']]],
  ['kflatplatemultdesc_481',['kFlatPlateMultDesc',['../class_aerofoil_editor.html#a60c8944c67bb2e7216fa78ceb69eda0c',1,'AerofoilEditor']]],
  ['kfullstalldesc_482',['kFullStallDesc',['../class_aerofoil_editor.html#aa221e93de9c7a0694496e168b52c3ab2',1,'AerofoilEditor']]],
  ['kmaxliftposdesc_483',['kMaxLiftPosDesc',['../class_aerofoil_editor.html#a253799dc0effda6f3c3057c9c6cb4582',1,'AerofoilEditor']]],
  ['kminliftposdesc_484',['kMinLiftPosDesc',['../class_aerofoil_editor.html#a9b6f6aa09c059e3b3305144114489eba',1,'AerofoilEditor']]],
  ['knegaoamultdesc_485',['kNegAoaMultDesc',['../class_aerofoil_editor.html#a17ad173fb09460d49a7f683708687dbe',1,'AerofoilEditor']]],
  ['kneutralliftdesc_486',['kNeutralLiftDesc',['../class_aerofoil_editor.html#aabefa5f1dcc53355281df9687fa03e4d',1,'AerofoilEditor']]]
];
