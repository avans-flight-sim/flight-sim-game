var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwyz~",
  1: "acgimopstuvw",
  2: "cu",
  3: "acgimopstuw",
  4: "abcdefghilmopqrstuy~",
  5: "_abcdefgiklmnopqrstuvwyz",
  6: "c",
  7: "abcdklrtvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Properties"
};

