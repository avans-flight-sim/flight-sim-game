var searchData=
[
  ['description_462',['description',['../class_aerofoil.html#a9ff1ca4713be454efaea5f4644214575',1,'Aerofoil']]],
  ['dimensions_463',['dimensions',['../class_wing.html#a7b5bc537cc6a8bb4119461a85e84d0f3',1,'Wing']]],
  ['double_5fformat_464',['DOUBLE_FORMAT',['../class_u_i_1_1_components_1_1_information_container_manager.html#a172d7bd8353cb5799d0d7b47d836bc73',1,'UI::Components::InformationContainerManager']]],
  ['double_5fround_5fdigits_5famount_465',['DOUBLE_ROUND_DIGITS_AMOUNT',['../class_u_i_1_1_components_1_1_information_container_manager.html#afb9cef1ef53657b44599fa4160a65adb',1,'UI::Components::InformationContainerManager']]],
  ['drag_466',['drag',['../class_aerofoil.html#a3e3b804a058e1500a5468294567982a6',1,'Aerofoil']]],
  ['dragcoefficient_467',['dragCoefficient',['../class_wing.html#a631de8ee546c34f23437f9654cd5fb00',1,'Wing']]],
  ['dragforce_468',['dragForce',['../class_wing.html#a2edbc7af7dba73f4f2ccc4066d8c1580',1,'Wing']]],
  ['dragmultiplier_469',['dragMultiplier',['../class_wing.html#a928dd07ada6c78d35f187388e11a7176',1,'Wing']]]
];
