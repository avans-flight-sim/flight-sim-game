var searchData=
[
  ['name_519',['NAME',['../class_u_i_1_1_components_1_1_go_back_popup_manager.html#a659c0c667c0ca416651a757949376ca3',1,'UI.Components.GoBackPopupManager.NAME()'],['../class_u_i_1_1_screens_1_1_about_screen_manager.html#aeaedfc5926078546d4c40b630fde0c2d',1,'UI.Screens.AboutScreenManager.NAME()'],['../class_u_i_1_1_screens_1_1_options_screen_manager.html#a96f46210414eb14047dbcd3a9c0fa338',1,'UI.Screens.OptionsScreenManager.NAME()'],['../class_u_i_1_1_screens_1_1_title_screen_manager.html#a84298d30e1d0ab17100cf952b22f4903',1,'UI.Screens.TitleScreenManager.NAME()']]],
  ['negativeaoamult_520',['negativeAoaMult',['../class_aerofoil_editor.html#a41b9381f8bf382b2a77925b0e7b58949',1,'AerofoilEditor']]],
  ['neutrallift_521',['neutralLift',['../class_aerofoil_editor.html#aa68e42b83e3c76aac4f304656edd5597',1,'AerofoilEditor']]],
  ['nl_5fculture_5fname_522',['NL_CULTURE_NAME',['../class_u_i_1_1_components_1_1_information_container_manager.html#aa41ad554ff90dd473737a195dbec7b84',1,'UI::Components::InformationContainerManager']]],
  ['no_523',['NO',['../class_u_i_1_1_components_1_1_go_back_popup_manager.html#a4f1c6c1d6ffd2f1e55461846de615097',1,'UI::Components::GoBackPopupManager']]]
];
