﻿using UI.Util;
using UnityEngine;

/// <summary>
/// Used for sharing information about the airplane
/// </summary>
public class AirplaneInformationSharer : MonoBehaviour
{
    /// <summary>
    /// Rigidbody of the airplane
    /// </summary>
    private Rigidbody _rigidbody;
    
    /// <summary>
    /// Sets the rigidbody field
    /// </summary>
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    /// <summary>
    /// Shares the information from the rigidbody
    /// </summary>
    private void Update()
    {
        AirplaneInformation.GetInstance().Altitude = _rigidbody.position.y;
        AirplaneInformation.GetInstance().Velocity = _rigidbody.velocity.magnitude;
    }
}