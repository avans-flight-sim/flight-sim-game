﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// The AerofoilEditor script is an inspector editor made for the Aerofoil script. 
/// The editor scrips houses and accepts all the different parameters needed to generate an appropriate lift curve.
/// Afterwards the liftcruve van be generated witht the "generate lift curve" button
/// </summary>
[CustomEditor(typeof(Aerofoil))]
public class AerofoilEditor : Editor
{


	/// <summary>
	/// Lift generated when the wing as it zero angle of attack.
	/// </summary>
	private float neutralLift = 0.0f;

	/// <summary>
	/// Lift coeffient at a positive, critical angle of attack, when the wing is generating the most lift.
	/// </summary>
	private float maxLiftPositive = 1.1f;

	/// <summary>
	/// Lift coeffient when the wing is fully stalled at a positive angle of attack.
	/// </summary>
	private float minLiftPositive = 0.6f;

	/// <summary>
	/// Multiplier for lift generated before stall when at negative angles of attack.
	/// </summary>
	private float negativeAoaMult = 1.0f;

	/// <summary>
	/// Multiplier for the flat plate lift that occurs for any wing from 45 to 135 degrees of rotation.
	/// </summary>
	private float flatPlateMult = 1.0f;

	/// <summary>
	/// Critical angle of attack is both the angle at which the wing starts to stall, and the angle at which it produces the most lift.
	/// </summary>
	private float criticalAngle = 16.0f;

	/// <summary>
	/// Angle of attack at which the wing is fully stalled and producing the minimum lift.
	/// </summary>
	private float fullyStalledAngle = 20.0f;

	const string kNeutralLiftDesc = "Lift generated when the wing as it zero angle of attack.";
	const string kCriticalDesc = "Critical angle of attack is both the angle at which the wing starts to stall, and the angle at which it produces the most lift.";
	const string kMaxLiftPosDesc = "Lift coeffient at a positive, critical angle of attack, when the wing is generating the most lift.";
	const string kMinLiftPosDesc = "Lift coeffient when the wing is fully stalled at a positive angle of attack.";
	const string kNegAoaMultDesc = "Multiplier for lift generated before stall when at negative angles of attack.";
	const string kFlatPlateMultDesc = "Multiplier for the flat plate lift that occurs for any wing from 45 to 135 degrees of rotation.";
	const string kFullStallDesc = "Angle of attack at which the wing is fully stalled and producing the minimum lift.";

	const float kflatPlateMax = 0.9f;

	/// <summary>
	/// Ran everytime there is interaction with the inspectorGUI
	/// </summary>
	public override void OnInspectorGUI()
    {
		//Standard unity stuff
        base.OnInspectorGUI();

        Aerofoil curve = (Aerofoil)target;

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Lift coefficient curve", EditorStyles.boldLabel);

		neutralLift = EditorGUILayout.FloatField(new GUIContent("Cl at Zero AOA: ", kNeutralLiftDesc), neutralLift);
		maxLiftPositive = EditorGUILayout.FloatField(new GUIContent("Cl at Critical AOA: ", kMaxLiftPosDesc), maxLiftPositive);
		minLiftPositive = EditorGUILayout.FloatField(new GUIContent("Cl when Fully Stalled: ", kMinLiftPosDesc), minLiftPositive);

		EditorGUILayout.Space();
		criticalAngle = EditorGUILayout.FloatField(new GUIContent("Critical AOA: ", kCriticalDesc), criticalAngle);
		fullyStalledAngle = EditorGUILayout.FloatField(new GUIContent("Fully Stalled AOA: ", kFullStallDesc), fullyStalledAngle);

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("LEAVE THESE AT 1 IF YOU DON'T KNOW WHAT THEY DO");
		flatPlateMult = EditorGUILayout.FloatField(new GUIContent("Flat Plate Lift Multiplier: ", kFlatPlateMultDesc), flatPlateMult);
		negativeAoaMult = EditorGUILayout.FloatField(new GUIContent("Negative AOA Multiplier: ", kNegAoaMultDesc), negativeAoaMult);


		// Error checking. Prevent keys from going in out of order and try to maintain a sorta normal line.
		if (fullyStalledAngle < criticalAngle)
			fullyStalledAngle = criticalAngle + 0.1f;

		if (neutralLift > maxLiftPositive)
			neutralLift = maxLiftPositive - 0.01f;

		flatPlateMult = Mathf.Clamp(flatPlateMult, 0.0f, 100.0f);

		List<Keyframe> keyList = new List<Keyframe>(9)
		{
			//Important, first part of the lift coefficient graph, consisting with the data added by the user
			new Keyframe(0.0f, neutralLift),
			new Keyframe(criticalAngle, maxLiftPositive),
			new Keyframe(fullyStalledAngle, minLiftPositive),

			// Flat plate, generic across all wings. Based on research from: http://www.aerospaceweb.org/question/airfoils/q0150b.shtml
			new Keyframe(45.0f, kflatPlateMax * flatPlateMult),
			new Keyframe(90.0f, 0.0f),
			new Keyframe(135.0f, -kflatPlateMax * flatPlateMult),

			// Wing at negative AOA. Also based on the same research as stated above.
			new Keyframe(180.0f - fullyStalledAngle, -minLiftPositive * negativeAoaMult),
			new Keyframe(180.0f - criticalAngle, -maxLiftPositive * negativeAoaMult),
			new Keyframe(180.0f, neutralLift)
		};

		EditorGUILayout.Space();

		bool shouldGenerate = GUILayout.Button("Generate lift curve");
		if (shouldGenerate)
		{
			curve.SetLiftCurve(keyList.ToArray());
			Repaint();
		}

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Drag coefficient curve", EditorStyles.boldLabel);
		EditorGUILayout.LabelField("The drag coefficient cruve cannot be custom made yet ");
		EditorGUILayout.LabelField("since there isn't enough research done to determine the values");
		EditorGUILayout.LabelField("at extreme AOA, so the standard drag curve will be used!");
	}
}
