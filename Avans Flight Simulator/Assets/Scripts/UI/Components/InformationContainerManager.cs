﻿using System;
using System.Globalization;
using UI.Util;
using UnityEngine.UIElements;

namespace UI.Components
{
    /// <summary>
    /// VisualElement that contains all the information about the airplane on the game screen
    /// </summary>
    public class InformationContainerManager : VisualElement, AirplaneInformation.ValuesChangedCallback
    {
        /// <summary>
        /// Name of the information label
        /// </summary>
        public const string INFORMATION = "Information";

        /// <summary>
        /// Format used to go from a double value to a string value
        /// </summary>
        private const string DOUBLE_FORMAT = "F";
        
        /// <summary>
        /// Culture used for the double to string format
        /// </summary>
        private const string NL_CULTURE_NAME = "nl-NL";

        /// <summary>
        /// How many digits should be showed of a double value
        /// </summary>
        private const int DOUBLE_ROUND_DIGITS_AMOUNT = 2;
        
        /// <summary>
        /// Maximum value of the percentage
        /// </summary>
        private const int PERCENTAGE_MAX_VALUE = 100;
        
        /// <summary>
        /// CultureInfo object of The Netherlands. Is used for formatting the double values to string values
        /// </summary>
        private CultureInfo _netherlandsCultureInfo;
        
        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public InformationContainerManager()
        {
            _netherlandsCultureInfo = CultureInfo.CreateSpecificCulture(NL_CULTURE_NAME);
            
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
            AirplaneInformation.GetInstance().RegisterCallback(this);
        }

        /// <summary>
        /// Unregisters the set callbacks in the constructor
        /// </summary>
        ~InformationContainerManager()
        {
            AirplaneInformation.GetInstance().UnregisterCallback(this);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<InformationContainerManager>
        {
        }

        /// <summary>
        /// Updates the information text that is being displayed
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            UpdateInformationLabel();
            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        public void OnVelocityChange(double velocity)
        {
            UpdateInformationLabel();
        }

        public void OnAltitudeChange(double altitude)
        {
            UpdateInformationLabel();
        }

        public void OnThrottleChange(float throttle)
        {
            UpdateInformationLabel();
        }

        public void OnBrakeChange(float brake)
        {
            UpdateInformationLabel();
        }

        /// <summary>
        /// Updates the text in the information label
        /// </summary>
        private void UpdateInformationLabel()
        {
            double velocityValue = Math.Round(AirplaneInformation.GetInstance().Velocity, DOUBLE_ROUND_DIGITS_AMOUNT);
            double altitudeValue = Math.Round(AirplaneInformation.GetInstance().Altitude, DOUBLE_ROUND_DIGITS_AMOUNT);
            double throttleValue = Math.Round(AirplaneInformation.GetInstance().Throttle * PERCENTAGE_MAX_VALUE);
            double brakeValue = Math.Round(AirplaneInformation.GetInstance().Brake * PERCENTAGE_MAX_VALUE);
        
            string text = $"Velocity: {velocityValue.ToString(DOUBLE_FORMAT, _netherlandsCultureInfo)} m/s\n" +
                          $"Altitude: {altitudeValue.ToString(DOUBLE_FORMAT, _netherlandsCultureInfo)} m\n" +
                          $"Throttle: {throttleValue} %\n" +
                          $"Brake: {brakeValue} %";

            this.Q<Label>(INFORMATION).text = text;
        }
    }
}