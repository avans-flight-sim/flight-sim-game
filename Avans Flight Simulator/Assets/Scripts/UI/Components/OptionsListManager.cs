﻿using UI.Util;
using UnityEngine.UIElements;

namespace UI.Components
{
    /// <summary>
    /// VisualElement that contains all the different options
    /// </summary>
    public class OptionsListManager : VisualElement
    {
        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public OptionsListManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<OptionsListManager>
        {
        }

        /// <summary>
        /// Sets all the options to the saved data of the options
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            OptionsManager.LoadIntoVisualElements(Children());

            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }
    }
}