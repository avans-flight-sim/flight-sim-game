﻿using UI.Util;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace UI.Components
{
    /// <summary>
    /// VisualElement that contains all information about the go back popup on the game screen
    /// </summary>
    public class GoBackPopupManager : VisualElement
    {
        /// <summary>
        /// Name of the visual element
        /// </summary>
        public const string NAME = "GoBackPopup";

        /// <summary>
        /// Name of the yes button
        /// </summary>
        public const string YES = "Yes";

        /// <summary>
        /// Name of the no button
        /// </summary>
        public const string NO = "No";

        /// <summary>
        /// Name of the scene that will be loaded by the popup
        /// </summary>
        private const string SCENE = "TitleScreen";

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public GoBackPopupManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<GoBackPopupManager>
        {
        }

        /// <summary>
        /// Registers the callbacks on the buttons in the popup
        /// </summary>
        /// <param name="evt"></param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            this.Q(YES)?.RegisterCallback<ClickEvent>(ev => GoBackToTitleScreen());
            this.Q(NO)?.RegisterCallback<ClickEvent>(ev => CancelGoBackToTitleScreen());

            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Callback that is called when the yes button has been pressed. Loads the title screen scene
        /// </summary>
        private void GoBackToTitleScreen()
        {
            SceneManager.LoadSceneAsync(SCENE);
        }

        /// <summary>
        /// Callback that is called when the no button has been pressed. Hides the go back popup
        /// </summary>
        private void CancelGoBackToTitleScreen()
        {
            GoBackPopup.GetInstance().Hide();
        }
    }
}