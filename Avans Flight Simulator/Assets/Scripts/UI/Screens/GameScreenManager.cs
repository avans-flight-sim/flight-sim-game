﻿using UI.Components;
using UI.Util;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

namespace UI.Screens
{
    /// <summary>
    /// VisualElement that represents the game screen
    /// </summary>
    public class GameScreenManager : VisualElement, GoBackPopup.InputCallback
    {
        /// <summary>
        /// Visual element object of the go back popup
        /// </summary>
        private VisualElement GoBackPopup;

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public GameScreenManager()
        {
            Util.GoBackPopup.GetInstance().RegisterCallback(this);
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Unregisters the callbacks that are used by this class
        /// </summary>
        ~GameScreenManager()
        {
            Util.GoBackPopup.GetInstance().UnregisterCallback(this);
        }

        /// <summary>
        /// Custom visual element that can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<GameScreenManager>
        {
        }

        /// <summary>
        /// Binds the visual elements to the VisualElement fields in this class
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            GoBackPopup = this.Q(GoBackPopupManager.NAME);

            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Hides the go back popup
        /// </summary>
        public void HideGoBackPopup()
        {
            Cursor.lockState = CursorLockMode.Locked;
            GoBackPopup.style.display = DisplayStyle.None;
        }

        /// <summary>
        /// Shows the go back popup
        /// </summary>
        public void ShowGoBackPopup()
        {
            Cursor.lockState = CursorLockMode.None;
            GoBackPopup.style.display = DisplayStyle.Flex;
        }
    }
}