﻿using UnityEngine.UIElements;

namespace UI.Screens
{
    /// <summary>
    /// Manages the different screens that can be displayed at the start of the application
    /// </summary>
    public class ScreenManager : VisualElement
    {
        /// <summary>
        /// VisualElement of the about screen
        /// </summary>
        private VisualElement AboutScreen;

        /// <summary>
        /// VisualElement of the title screen
        /// </summary>
        private VisualElement TitleScreen;

        /// <summary>
        /// VisualElement of the options screen
        /// </summary>
        private VisualElement OptionsScreen;

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public ScreenManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<ScreenManager>
        {
        }

        /// <summary>
        /// Registers the callbacks of the buttons
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            TitleScreen = this.Q(TitleScreenManager.NAME);
            OptionsScreen = this.Q(OptionsScreenManager.NAME);
            AboutScreen = this.Q(AboutScreenManager.NAME);

            TitleScreen?.Q(TitleScreenManager.OPTIONS)?.RegisterCallback<ClickEvent>(ev => EnableOptionsScreen());
            TitleScreen?.Q(TitleScreenManager.ABOUT)?.RegisterCallback<ClickEvent>(ev => EnableAboutScreen());
            OptionsScreen?.Q(OptionsScreenManager.BACK)?.RegisterCallback<ClickEvent>(ev => EnableTitleScreen());
            OptionsScreen?.Q(OptionsScreenManager.SAVE)?.RegisterCallback<ClickEvent>(ev => EnableTitleScreen());
            AboutScreen?.Q(AboutScreenManager.BACK)?.RegisterCallback<ClickEvent>(ev => EnableTitleScreen());

            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Shows the title screen and makes the options and about screen invisible
        /// </summary>
        private void EnableTitleScreen()
        {
            TitleScreen.style.display = DisplayStyle.Flex;
            OptionsScreen.style.display = DisplayStyle.None;
            AboutScreen.style.display = DisplayStyle.None;
        }

        /// <summary>
        /// Shows the options screen and makes the title and about screen invisible
        /// </summary>
        private void EnableOptionsScreen()
        {
            OptionsScreen.style.display = DisplayStyle.Flex;
            TitleScreen.style.display = DisplayStyle.None;
            AboutScreen.style.display = DisplayStyle.None;
        }

        /// <summary>
        /// Shows the about screen and makes the options and title screen invisible
        /// </summary>
        private void EnableAboutScreen()
        {
            AboutScreen.style.display = DisplayStyle.Flex;
            TitleScreen.style.display = DisplayStyle.None;
            OptionsScreen.style.display = DisplayStyle.None;
        }
    }
}