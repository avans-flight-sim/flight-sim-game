﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace UI.Screens
{
    /// <summary>
    /// VisualElement that represents the title screen
    /// </summary>
    public class TitleScreenManager : VisualElement
    {
        /// <summary>
        /// Name of the screen
        /// </summary>
        public const string NAME = "TitleScreen";

        /// <summary>
        /// Name of the start button
        /// </summary>
        public const string START = "Start";

        /// <summary>
        /// Name of the quit button
        /// </summary>
        public const string QUIT = "Quit";

        /// <summary>
        /// Name of the options button
        /// </summary>
        public const string OPTIONS = "Options";

        /// <summary>
        /// Name of the about button
        /// </summary>
        public const string ABOUT = "About";

        /// <summary>
        /// Name of scene that will be loaded when the game is started after the start button is clicked
        /// </summary>
        private const string SCENE = "Terrain";

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public TitleScreenManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<TitleScreenManager>
        {
        }

        /// <summary>
        /// Registers the callbacks of the buttons
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            this.Q(START)?.RegisterCallback<ClickEvent>(ev => StartGame());
            this.Q(QUIT)?.RegisterCallback<ClickEvent>(ev => QuitGame());

            UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Starts the game
        /// </summary>
        private void StartGame()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
#endif
                SceneManager.LoadSceneAsync(SCENE);
#if UNITY_EDITOR
            else
                Debug.Log("Loading: " + SCENE);
#endif
        }

        /// <summary>
        /// Closes the application
        /// </summary>
        private void QuitGame()
        {
#if UNITY_EDITOR
            Debug.Log("Quit Game");
#endif
            Application.Quit();
        }
    }
}