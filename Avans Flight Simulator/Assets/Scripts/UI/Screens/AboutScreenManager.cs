﻿using UnityEngine.UIElements;

namespace UI.Screens
{
    /// <summary>
    /// VisualElement that represents the about screen
    /// </summary>
    public class AboutScreenManager : VisualElement
    {
        /// <summary>
        /// Name of the screen
        /// </summary>
        public const string NAME = "AboutScreen";

        /// <summary>
        /// Name of the back button
        /// </summary>
        public const string BACK = "Back";

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public AboutScreenManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<AboutScreenManager>
        {
        }

        /// <summary>
        /// Registers the callbacks of the buttons
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }
    }
}