﻿using UI.Util;
using UnityEngine.UIElements;

namespace UI.Screens
{
    /// <summary>
    /// VisualElement that represents the options screen
    /// </summary>
    public class OptionsScreenManager : VisualElement
    {
        /// <summary>
        /// Name of the screen
        /// </summary>
        public const string NAME = "OptionsScreen";

        /// <summary>
        /// Name of the save button
        /// </summary>
        public const string SAVE = "Save";

        /// <summary>
        /// Name of the back button
        /// </summary>
        public const string BACK = "Back";

        /// <summary>
        /// Name of the options list VisualElement
        /// </summary>
        public const string OPTIONS_LIST = "OptionsList";

        /// <summary>
        /// Registers the callbacks that are used by this class
        /// </summary>
        public OptionsScreenManager()
        {
            this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Custom visual element can be used in the UXML files
        /// </summary>
        public new class UxmlFactory : UxmlFactory<OptionsScreenManager>
        {
        }

        /// <summary>
        /// Registers the callbacks of the buttons
        /// </summary>
        /// <param name="evt">Event that contains all the old and new geometry data</param>
        private void OnGeometryChange(GeometryChangedEvent evt)
        {
            this.Q(SAVE)?.RegisterCallback<ClickEvent>(ev => SaveOptions());
            this.Q(BACK)?.RegisterCallback<ClickEvent>(ev => DiscardInput());

            this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
        }

        /// <summary>
        /// Saves all the options
        /// </summary>
        private void SaveOptions()
        {
            OptionsManager.Save(this.Q(OPTIONS_LIST)?.Children());
        }

        /// <summary>
        /// Discards the input and changes the options back to the last saved values
        /// </summary>
        private void DiscardInput()
        {
            OptionsManager.LoadIntoVisualElements(this.Q(OPTIONS_LIST)?.Children());
        }
    }
}