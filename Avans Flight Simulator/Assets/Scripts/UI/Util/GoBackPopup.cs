﻿using System.Collections.Generic;

namespace UI.Util
{
    public class GoBackPopup
    {
        /// <summary>
        /// Callbacks that will be called when a value changes
        /// </summary>
        private readonly List<InputCallback> _callbacks = new List<InputCallback>();

        private GoBackPopup()
        {
        }

        /// <summary>
        /// The only instance of the AirplaneInformation
        /// </summary>
        private static GoBackPopup _instance;

        /// <summary>
        /// Lock to make the singleton thread safe
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// Returns the only instance of AirplaneInformation
        /// </summary>
        /// <returns>AirplaneInformation instance</returns>
        public static GoBackPopup GetInstance()
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                    {
                        _instance = new GoBackPopup();
                    }
                }
            }

            return _instance;
        }

        /// <summary>
        /// Register a callback that will be called when the popup is shown or hidden
        /// </summary>
        /// <param name="callback">Callback that will be called</param>
        public void RegisterCallback(InputCallback callback)
        {
            _callbacks.Add(callback);
        }

        /// <summary>
        /// Unregister a callback that would be called when the popup is shown or hidden
        /// </summary>
        /// <param name="callback">Callback that would be called</param>
        public void UnregisterCallback(InputCallback callback)
        {
            _callbacks.Remove(callback);
        }
        
        /// <summary>
        /// Show the callback
        /// </summary>
        public void Show()
        {
            _callbacks.ForEach(callback => callback.ShowGoBackPopup());
        }

        /// <summary>
        /// Hide the callback
        /// </summary>
        public void Hide()
        {
            _callbacks.ForEach(callback => callback.HideGoBackPopup());
        }
        
        /// <summary>
        /// Callback when the go back popup is shown or hidden
        /// </summary>
        public interface InputCallback
        {
            void HideGoBackPopup();
            void ShowGoBackPopup();
        }
    }
}