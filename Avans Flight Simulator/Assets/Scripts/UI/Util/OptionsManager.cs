﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI.Util
{
    /// <summary>
    /// Manager for all the options that are used in this project
    /// </summary>
    public static class OptionsManager
    {
        /// <summary>
        /// Name of the text field
        /// </summary>
        public const string TEXT_FIELD = "TextField";

        /// <summary>
        /// Name of the toggle
        /// </summary>
        public const string TOGGLE = "Toggle";

        /// <summary>
        /// Name of the slider
        /// </summary>
        public const string SLIDER = "Slider";

        /// <summary>
        /// Loads the saved options data into the visual elements on the UI
        /// </summary>
        /// <param name="options">The options of the UI</param>
        public static void LoadIntoVisualElements(IEnumerable<VisualElement> options)
        {
            foreach (var option in options)
            {
                var textField = option.Q<TextField>(TEXT_FIELD);
                if (textField != null)
                {
                    LoadTextFieldOption(option.name, textField);
                    continue;
                }

                var toggle = option.Q<Toggle>(TOGGLE);
                if (toggle != null)
                {
                    LoadToggleOption(option.name, toggle);
                    continue;
                }

                var slider = option.Q<Slider>(SLIDER);
                if (slider != null)
                {
                    LoadSliderOption(option.name, slider);
                    continue;
                }

                Debug.LogError($"OPTION NOT USING {TEXT_FIELD}, {TOGGLE} or {SLIDER}");
            }
        }

        /// <summary>
        /// Sets the text field to the saved value
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="textField">TextField component that will be set according to the saved value of the option</param>
        private static void LoadTextFieldOption(string optionName, [NotNull] TextField textField)
        {
            textField.value = GetStringValue(optionName);
        }

        /// <summary>
        /// Sets the toggle to the saved value
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="toggle">Toggle component that will be toggled according to the saved value of the option</param>
        private static void LoadToggleOption(string optionName, [NotNull] Toggle toggle)
        {
            toggle.value = GetBoolValue(optionName);
        }

        /// <summary>
        /// Sets the slider to the saved value
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="slider">Slider component that will be set according to the saved value of the option</param>
        private static void LoadSliderOption(string optionName, [NotNull] Slider slider)
        {
            slider.value = GetFloatValue(optionName);
        }

        /// <summary>
        /// Saves the values of the VisualElement options
        /// </summary>
        /// <param name="options">The options that are present on the UI</param>
        public static void Save(in IEnumerable<VisualElement> options)
        {
            foreach (var option in options)
            {
                var textField = option.Q<TextField>(TEXT_FIELD);
                if (textField != null)
                {
                    SaveTextFieldOption(option.name, textField);
                    continue;
                }

                var toggle = option.Q<Toggle>(TOGGLE);
                if (toggle != null)
                {
                    SaveToggleOption(option.name, toggle);
                    continue;
                }

                var slider = option.Q<Slider>(SLIDER);
                if (slider != null)
                {
                    SaveSliderOption(option.name, slider);
                    continue;
                }

                Debug.LogError($"OPTION NOT USING {TEXT_FIELD}, {TOGGLE} or {SLIDER}");
            }

            PlayerPrefs.Save();
        }

        /// <summary>
        /// Saves the text field value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="textField">TextField component that is holding the value</param>
        private static void SaveTextFieldOption(string optionName, [NotNull] TextField textField)
        {
            PlayerPrefs.SetString(optionName, textField.value);
        }

        /// <summary>
        /// Saves the toggle value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="toggle">Toggle component that is holding the value</param>
        private static void SaveToggleOption(string optionName, [NotNull] Toggle toggle)
        {
            var toggleValue = toggle.value ? 1 : 0;
            PlayerPrefs.SetInt(optionName, toggleValue);
        }

        /// <summary>
        /// Saves the slider value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <param name="slider">Slider component that is holding the value</param>
        private static void SaveSliderOption(string optionName, [NotNull] Slider slider)
        {
            PlayerPrefs.SetFloat(optionName, slider.value);
        }

        /// <summary>
        /// Receive the saved string value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <returns></returns>
        public static string GetStringValue(string optionName)
        {
            return PlayerPrefs.GetString(optionName);
        }

        /// <summary>
        /// Receive the saved int value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <returns></returns>
        public static int GetIntValue(string optionName)
        {
            return PlayerPrefs.GetInt(optionName);
        }

        /// <summary>
        /// Receive the saved float value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <returns></returns>
        public static float GetFloatValue(string optionName)
        {
            return PlayerPrefs.GetFloat(optionName);
        }

        /// <summary>
        /// Receive the saved bool value of the given option
        /// </summary>
        /// <param name="optionName">Name of the option</param>
        /// <returns></returns>
        public static bool GetBoolValue(string optionName)
        {
            return GetIntValue(optionName) == 1;
        }
    }
}