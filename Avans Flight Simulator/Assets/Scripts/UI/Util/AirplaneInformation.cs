﻿using System.Collections.Generic;

namespace UI.Util
{
    /// <summary>
    /// Contains all the information about the airplane
    /// </summary>
    public class AirplaneInformation
    {
        /// <summary>
        /// Velocity of the airplane
        /// </summary>
        private double _velocity;

        public double Velocity
        {
            get => _velocity;
            set { _callbacks.ForEach(callback => callback.OnVelocityChange(_velocity = value)); }
        }

        /// <summary>
        /// Altitude of the airplane
        /// </summary>
        private double _altitude;

        public double Altitude
        {
            get => _altitude;
            set { _callbacks.ForEach(callback => callback.OnAltitudeChange(_altitude = value)); }
        }

        /// <summary>
        /// Throttle value of the airplane
        /// </summary>
        private float _throttle;

        public float Throttle
        {
            get => _throttle;
            set { _callbacks.ForEach(callback => callback.OnThrottleChange(_throttle = value)); }
        }

        /// <summary>
        /// Brake value of the airplane
        /// </summary>
        private float _brake;

        public float Brake
        {
            get => _brake;
            set { _callbacks.ForEach(callback => callback.OnBrakeChange(_brake = value)); }
        }

        /// <summary>
        /// Callbacks that will be called when a value changes
        /// </summary>
        private readonly List<ValuesChangedCallback> _callbacks = new List<ValuesChangedCallback>();

        private AirplaneInformation()
        {
        }

        /// <summary>
        /// The only instance of the AirplaneInformation
        /// </summary>
        private static AirplaneInformation _instance;

        /// <summary>
        /// Lock to make the singleton thread safe
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// Returns the only instance of AirplaneInformation
        /// </summary>
        /// <returns>AirplaneInformation instance</returns>
        public static AirplaneInformation GetInstance()
        {
            if (_instance == null)
            {
                lock (Lock)
                {
                    if (_instance == null)
                    {
                        _instance = new AirplaneInformation();
                    }
                }
            }

            return _instance;
        }

        /// <summary>
        /// Register a callback that will be called when a value changes
        /// </summary>
        /// <param name="callback">Callback that will be called</param>
        public void RegisterCallback(ValuesChangedCallback callback)
        {
            _callbacks.Add(callback);
        }

        /// <summary>
        /// Unregister a callback that will be called when a value changes
        /// </summary>
        /// <param name="callback">Callback that was being called</param>
        public void UnregisterCallback(ValuesChangedCallback callback)
        {
            _callbacks.Remove(callback);
        }

        /// <summary>
        /// Callback that is used when a value of AirplaneInformation changes
        /// </summary>
        public interface ValuesChangedCallback
        {
            /// <summary>
            /// Called when the velocity of the airplane changes
            /// </summary>
            /// <param name="velocity">The new velocity of the airplane</param>
            void OnVelocityChange(double velocity);

            /// <summary>
            /// Called when the altitude of the airplane changes
            /// </summary>
            /// <param name="altitude">The new altitude of the airplane</param>
            void OnAltitudeChange(double altitude);

            /// <summary>
            /// Called when the throttle value of the airplane changes
            /// </summary>
            /// <param name="throttle">The new throttle value of the airplane</param>
            void OnThrottleChange(float throttle);

            /// <summary>
            /// Called when the brake value of the airplane changes
            /// </summary>
            /// <param name="brake">The new brake value of the airplane</param>
            void OnBrakeChange(float brake);
        }
    }
}