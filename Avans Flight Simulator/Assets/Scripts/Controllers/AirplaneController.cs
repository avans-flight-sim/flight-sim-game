﻿using UI.Util;

namespace Controllers
{
    /// <summary>
    /// Controller used for all the input concerning the airplane
    /// </summary>
    public class AirplaneController : InputController
    {
        /// <summary>
        /// Sets the methods to it's actions
        /// </summary>
        private void Start()
        {
            MasterControls.Airplane.Flaps.performed += context => Flaps();
        }

        /// <summary>
        /// Checks if there is being throttled, braked, pitched, rolled or yawed
        /// </summary>
        private void Update()
        {
            Throttle();
            Brake();
            Pitch();
            Roll();
            Yaw();
        }

        /// <summary>
        /// Method that will be called when the flaps are toggled
        /// </summary>
        private void Flaps()
        {
            //TODO: Implement logic
        }

        /// <summary>
        /// Method that will be called when there is being throttled
        /// </summary>
        private void Throttle()
        {
            float throttle = MasterControls.Airplane.Throttle.ReadValue<float>();

            AirplaneInformation.GetInstance().Throttle = throttle; //TODO: is this right?

            //TODO: Implement logic
        }

        /// <summary>
        /// Method that will be called when there is being braked
        /// </summary>
        private void Brake()
        {
            float brake = MasterControls.Airplane.Brake.ReadValue<float>();

            AirplaneInformation.GetInstance().Brake = brake;

            //TODO: Implement logic
        }

        /// <summary>
        /// Method that will be called when there is being pitched
        /// </summary>
        private void Pitch()
        {
            float pitch = MasterControls.Airplane.Pitch.ReadValue<float>();

            //TODO: Implement logic
        }

        /// <summary>
        /// Method that will be called when there is being rolled
        /// </summary>
        private void Roll()
        {
            float roll = MasterControls.Airplane.Roll.ReadValue<float>();

            //TODO: Implement logic
        }

        /// <summary>
        /// method that will be called when there is being yawed
        /// </summary>
        private void Yaw()
        {
            float yaw = MasterControls.Airplane.Yaw.ReadValue<float>();

            //TODO: Implement logic
        }

        //Example:
        //
        //private void Move()
        //{
        //    // Read the movement value
        //    Vector2 direction = masterControls.Airplane.Move.ReadValue<Vector2>();
        //
        //    // Move the player
        //    Vector3 currentPosition = transform.position;
        //    currentPosition.x += direction.x * speed * Time.deltaTime;
        //    currentPosition.z += direction.y * speed * Time.deltaTime;
        //    transform.position = currentPosition;
        //}
    }
}