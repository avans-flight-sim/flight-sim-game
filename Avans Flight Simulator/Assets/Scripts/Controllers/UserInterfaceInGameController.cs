﻿using UI.Util;
using UnityEngine.SceneManagement;

namespace Controllers
{
    /// <summary>
    /// Controller used for all the input concerning the User Interface in game
    /// </summary>
    public class UserInterfaceInGameController : InputController
    {
        /// <summary>
        /// Sets the methods to it's actions
        /// </summary>
        private void Start()
        {
            MasterControls.UserInterfaceInGame.Backtotitlescreen.performed += context => BackToTitleScreen();
        }

        private void BackToTitleScreen()
        {
            // GoBackPopup.GetInstance().Show(); Todo: Fix, bugged out for one reason? Buttons don't react on button press
            SceneManager.LoadSceneAsync("TitleScreen");
        }
    }
}