﻿using UnityEngine;

namespace Controllers
{
    /// <summary>
    /// Standard controller that is used
    /// </summary>
    public abstract class InputController : MonoBehaviour
    {
        /// <summary>
        /// Includes all possible actions and is used to connect actions to it's method
        /// </summary>
        protected MasterControls MasterControls;

        /// <summary>
        /// Creates the MasterControls
        /// </summary>
        private void Awake() => MasterControls = new MasterControls();

        /// <summary>
        /// Enables the master controls
        /// </summary>
        private void OnEnable() => MasterControls.Enable();

        /// <summary>
        /// Disables the master controls
        /// </summary>
        private void OnDisable() => MasterControls.Disable();
    }
}