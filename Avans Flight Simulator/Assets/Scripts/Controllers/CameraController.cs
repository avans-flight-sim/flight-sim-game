﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Controllers
{
    /// <summary>
    /// Controller used for all the input concerning the camera
    /// </summary>
    public class CameraController : InputController
    {
        /// <summary>
        /// Sensitivity of the camera movement
        /// </summary>
        [FormerlySerializedAs("Sensitivity")] [SerializeField]
        private float sensitivity = 100f;

        /// <summary>
        /// Rotation cap for the x axis so you can not infinitely rotate up or down
        /// </summary>
        [FormerlySerializedAs("RotationCapXAxis")] [SerializeField]
        private float rotationCapXAxis = 90f;

        private float _currentRotationX = 0f;
        private float _currentRotationY = 0f;

        /// <summary>
        /// Fixes the cursor to locked mode
        /// </summary>
        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        /// <summary>
        /// Will check if the camera is being moved
        /// </summary>
        private void Update()
        {
            Look();
        }

        /// <summary>
        /// Calculates and sets the new position of the camera
        /// </summary>
        private void Look()
        {
            float inputRotationX = MasterControls.Camera.RotationX.ReadValue<float>();
            float inputRotationY = MasterControls.Camera.RotationY.ReadValue<float>();

            inputRotationX = inputRotationX * sensitivity * Time.deltaTime;
            inputRotationY = inputRotationY * sensitivity * Time.deltaTime;

            _currentRotationY += inputRotationX;
            _currentRotationX -= inputRotationY;
            _currentRotationX = Mathf.Clamp(_currentRotationX, -rotationCapXAxis, rotationCapXAxis);

            transform.localRotation = Quaternion.Euler(_currentRotationX, _currentRotationY, 0f);
        }
    }
}