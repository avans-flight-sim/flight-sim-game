// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Controllers/MasterControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MasterControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MasterControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MasterControls"",
    ""maps"": [
        {
            ""name"": ""Airplane"",
            ""id"": ""605a371e-3d8a-40fa-9735-e37ed6ef45b7"",
            ""actions"": [
                {
                    ""name"": ""Pitch"",
                    ""type"": ""Button"",
                    ""id"": ""e0444a6f-61b1-4601-a31a-7553728768db"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Roll"",
                    ""type"": ""Button"",
                    ""id"": ""e2d042d5-1e58-401a-ad23-29b12d82df85"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Yaw"",
                    ""type"": ""Button"",
                    ""id"": ""bcf69ec7-cb3f-4c37-ac01-8a916f66de6d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Flaps"",
                    ""type"": ""Button"",
                    ""id"": ""e742ed74-6c98-43f9-aa8f-e0ac766f1389"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Brake"",
                    ""type"": ""Button"",
                    ""id"": ""0eac4753-ce7f-4234-a16c-f7f022cae014"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Throttle"",
                    ""type"": ""Button"",
                    ""id"": ""db887eb5-7797-4e7b-8b7f-5294bc996d10"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""21ad5a52-1ddd-4464-a438-068710be22b4"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pitch"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""09e8b940-e3c9-4b76-9696-8c7a05139f5f"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Pitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""6d399353-332d-4962-8156-957cc404ce71"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Pitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""fb32ebf8-8df6-49d9-987b-c4dd6647cfd7"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pitch"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""97e5ed50-9db5-4b12-b44d-b28a4bb8d0fd"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Pitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a6afd3b4-ce54-413a-ab28-686317a63532"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Pitch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""8fa71981-f75e-42ec-bbbd-a029c4ed3468"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Yaw"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""7ca2573a-4428-46bb-8cf1-87e5d38061c7"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Yaw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""06ddf125-c5c0-4ba0-bac7-ab32cbaed846"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Yaw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""86278237-5aff-41e0-975c-229808d8cb0e"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Yaw"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""0b131473-3399-4e1d-9427-134fa3d9b02e"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Yaw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""81ea0fce-a8eb-45fb-9666-ceda48bd0278"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Yaw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""174a3dde-1abd-4589-8cbd-76a00224db0e"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Roll"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""32ebdd0b-bc67-446d-a313-8126ea94277d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a67ded49-b27f-4b40-b94b-04b8b1a3ed7f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""7ef22015-96f7-463b-97b5-ba5300e3062c"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Roll"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""918fa08c-72a1-42b8-a7ca-5a0fd4f4f86e"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""2df2424e-0218-4a96-ae2b-d74a25fa90ea"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Roll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""7fbc2989-f799-4116-9a42-4c44109ed48f"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Flaps"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6240c967-d22d-45c7-b493-9a783c94e12f"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Flaps"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9e57ca19-fc95-4be2-a3d2-3d4716abf919"",
                    ""path"": ""<Keyboard>/b"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Brake"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c4628e5-d56a-4c57-a41a-2774dcb38bef"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Brake"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""623a027a-699f-4eea-b41c-721f09a01989"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f6167157-2490-426c-931b-ddb3a1a46995"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Camera"",
            ""id"": ""c3489f8f-6424-4b5b-b0fe-c82d232221c7"",
            ""actions"": [
                {
                    ""name"": ""Rotation X"",
                    ""type"": ""Value"",
                    ""id"": ""11fb27de-3a3c-41f4-a26e-83b7a4850f30"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rotation Y"",
                    ""type"": ""Value"",
                    ""id"": ""f35e0046-1cf9-4812-94bd-b75865419c51"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5c69ab64-0245-44b3-9040-4274feab2011"",
                    ""path"": ""<Gamepad>/rightStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Rotation X"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d93f4720-d2ea-475e-a9ed-e6819b051935"",
                    ""path"": ""<Mouse>/delta/X"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Rotation X"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98c87de3-64a1-42f2-9cfe-9fb262e9abaf"",
                    ""path"": ""<Gamepad>/rightStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Rotation Y"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""68e77c8d-d12a-4f2a-baff-30fafb3f620e"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Rotation Y"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UserInterfaceInGame"",
            ""id"": ""51194378-75bd-4e89-8fc2-86a72ad7909b"",
            ""actions"": [
                {
                    ""name"": ""Back to title screen"",
                    ""type"": ""Button"",
                    ""id"": ""a9797aa6-d714-4cd2-99e3-331a7a69d78e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""844a6bac-35a2-494c-a6b6-befd82e0b63f"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and mouse"",
                    ""action"": ""Back to title screen"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dedf1c66-de3f-41ef-81fd-dab04fa5063f"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Back to title screen"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard and mouse"",
            ""bindingGroup"": ""Keyboard and mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Controller"",
            ""bindingGroup"": ""Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Airplane
        m_Airplane = asset.FindActionMap("Airplane", throwIfNotFound: true);
        m_Airplane_Pitch = m_Airplane.FindAction("Pitch", throwIfNotFound: true);
        m_Airplane_Roll = m_Airplane.FindAction("Roll", throwIfNotFound: true);
        m_Airplane_Yaw = m_Airplane.FindAction("Yaw", throwIfNotFound: true);
        m_Airplane_Flaps = m_Airplane.FindAction("Flaps", throwIfNotFound: true);
        m_Airplane_Brake = m_Airplane.FindAction("Brake", throwIfNotFound: true);
        m_Airplane_Throttle = m_Airplane.FindAction("Throttle", throwIfNotFound: true);
        // Camera
        m_Camera = asset.FindActionMap("Camera", throwIfNotFound: true);
        m_Camera_RotationX = m_Camera.FindAction("Rotation X", throwIfNotFound: true);
        m_Camera_RotationY = m_Camera.FindAction("Rotation Y", throwIfNotFound: true);
        // UserInterfaceInGame
        m_UserInterfaceInGame = asset.FindActionMap("UserInterfaceInGame", throwIfNotFound: true);
        m_UserInterfaceInGame_Backtotitlescreen = m_UserInterfaceInGame.FindAction("Back to title screen", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Airplane
    private readonly InputActionMap m_Airplane;
    private IAirplaneActions m_AirplaneActionsCallbackInterface;
    private readonly InputAction m_Airplane_Pitch;
    private readonly InputAction m_Airplane_Roll;
    private readonly InputAction m_Airplane_Yaw;
    private readonly InputAction m_Airplane_Flaps;
    private readonly InputAction m_Airplane_Brake;
    private readonly InputAction m_Airplane_Throttle;
    public struct AirplaneActions
    {
        private @MasterControls m_Wrapper;
        public AirplaneActions(@MasterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Pitch => m_Wrapper.m_Airplane_Pitch;
        public InputAction @Roll => m_Wrapper.m_Airplane_Roll;
        public InputAction @Yaw => m_Wrapper.m_Airplane_Yaw;
        public InputAction @Flaps => m_Wrapper.m_Airplane_Flaps;
        public InputAction @Brake => m_Wrapper.m_Airplane_Brake;
        public InputAction @Throttle => m_Wrapper.m_Airplane_Throttle;
        public InputActionMap Get() { return m_Wrapper.m_Airplane; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(AirplaneActions set) { return set.Get(); }
        public void SetCallbacks(IAirplaneActions instance)
        {
            if (m_Wrapper.m_AirplaneActionsCallbackInterface != null)
            {
                @Pitch.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnPitch;
                @Pitch.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnPitch;
                @Pitch.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnPitch;
                @Roll.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnRoll;
                @Roll.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnRoll;
                @Roll.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnRoll;
                @Yaw.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnYaw;
                @Yaw.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnYaw;
                @Yaw.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnYaw;
                @Flaps.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnFlaps;
                @Flaps.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnFlaps;
                @Flaps.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnFlaps;
                @Brake.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnBrake;
                @Brake.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnBrake;
                @Brake.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnBrake;
                @Throttle.started -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnThrottle;
                @Throttle.performed -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnThrottle;
                @Throttle.canceled -= m_Wrapper.m_AirplaneActionsCallbackInterface.OnThrottle;
            }
            m_Wrapper.m_AirplaneActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Pitch.started += instance.OnPitch;
                @Pitch.performed += instance.OnPitch;
                @Pitch.canceled += instance.OnPitch;
                @Roll.started += instance.OnRoll;
                @Roll.performed += instance.OnRoll;
                @Roll.canceled += instance.OnRoll;
                @Yaw.started += instance.OnYaw;
                @Yaw.performed += instance.OnYaw;
                @Yaw.canceled += instance.OnYaw;
                @Flaps.started += instance.OnFlaps;
                @Flaps.performed += instance.OnFlaps;
                @Flaps.canceled += instance.OnFlaps;
                @Brake.started += instance.OnBrake;
                @Brake.performed += instance.OnBrake;
                @Brake.canceled += instance.OnBrake;
                @Throttle.started += instance.OnThrottle;
                @Throttle.performed += instance.OnThrottle;
                @Throttle.canceled += instance.OnThrottle;
            }
        }
    }
    public AirplaneActions @Airplane => new AirplaneActions(this);

    // Camera
    private readonly InputActionMap m_Camera;
    private ICameraActions m_CameraActionsCallbackInterface;
    private readonly InputAction m_Camera_RotationX;
    private readonly InputAction m_Camera_RotationY;
    public struct CameraActions
    {
        private @MasterControls m_Wrapper;
        public CameraActions(@MasterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @RotationX => m_Wrapper.m_Camera_RotationX;
        public InputAction @RotationY => m_Wrapper.m_Camera_RotationY;
        public InputActionMap Get() { return m_Wrapper.m_Camera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CameraActions set) { return set.Get(); }
        public void SetCallbacks(ICameraActions instance)
        {
            if (m_Wrapper.m_CameraActionsCallbackInterface != null)
            {
                @RotationX.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationX;
                @RotationX.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationX;
                @RotationX.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationX;
                @RotationY.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationY;
                @RotationY.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationY;
                @RotationY.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnRotationY;
            }
            m_Wrapper.m_CameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @RotationX.started += instance.OnRotationX;
                @RotationX.performed += instance.OnRotationX;
                @RotationX.canceled += instance.OnRotationX;
                @RotationY.started += instance.OnRotationY;
                @RotationY.performed += instance.OnRotationY;
                @RotationY.canceled += instance.OnRotationY;
            }
        }
    }
    public CameraActions @Camera => new CameraActions(this);

    // UserInterfaceInGame
    private readonly InputActionMap m_UserInterfaceInGame;
    private IUserInterfaceInGameActions m_UserInterfaceInGameActionsCallbackInterface;
    private readonly InputAction m_UserInterfaceInGame_Backtotitlescreen;
    public struct UserInterfaceInGameActions
    {
        private @MasterControls m_Wrapper;
        public UserInterfaceInGameActions(@MasterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Backtotitlescreen => m_Wrapper.m_UserInterfaceInGame_Backtotitlescreen;
        public InputActionMap Get() { return m_Wrapper.m_UserInterfaceInGame; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UserInterfaceInGameActions set) { return set.Get(); }
        public void SetCallbacks(IUserInterfaceInGameActions instance)
        {
            if (m_Wrapper.m_UserInterfaceInGameActionsCallbackInterface != null)
            {
                @Backtotitlescreen.started -= m_Wrapper.m_UserInterfaceInGameActionsCallbackInterface.OnBacktotitlescreen;
                @Backtotitlescreen.performed -= m_Wrapper.m_UserInterfaceInGameActionsCallbackInterface.OnBacktotitlescreen;
                @Backtotitlescreen.canceled -= m_Wrapper.m_UserInterfaceInGameActionsCallbackInterface.OnBacktotitlescreen;
            }
            m_Wrapper.m_UserInterfaceInGameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Backtotitlescreen.started += instance.OnBacktotitlescreen;
                @Backtotitlescreen.performed += instance.OnBacktotitlescreen;
                @Backtotitlescreen.canceled += instance.OnBacktotitlescreen;
            }
        }
    }
    public UserInterfaceInGameActions @UserInterfaceInGame => new UserInterfaceInGameActions(this);
    private int m_KeyboardandmouseSchemeIndex = -1;
    public InputControlScheme KeyboardandmouseScheme
    {
        get
        {
            if (m_KeyboardandmouseSchemeIndex == -1) m_KeyboardandmouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard and mouse");
            return asset.controlSchemes[m_KeyboardandmouseSchemeIndex];
        }
    }
    private int m_ControllerSchemeIndex = -1;
    public InputControlScheme ControllerScheme
    {
        get
        {
            if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.FindControlSchemeIndex("Controller");
            return asset.controlSchemes[m_ControllerSchemeIndex];
        }
    }
    public interface IAirplaneActions
    {
        void OnPitch(InputAction.CallbackContext context);
        void OnRoll(InputAction.CallbackContext context);
        void OnYaw(InputAction.CallbackContext context);
        void OnFlaps(InputAction.CallbackContext context);
        void OnBrake(InputAction.CallbackContext context);
        void OnThrottle(InputAction.CallbackContext context);
    }
    public interface ICameraActions
    {
        void OnRotationX(InputAction.CallbackContext context);
        void OnRotationY(InputAction.CallbackContext context);
    }
    public interface IUserInterfaceInGameActions
    {
        void OnBacktotitlescreen(InputAction.CallbackContext context);
    }
}
