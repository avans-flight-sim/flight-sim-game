﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic cameraHandler script
/// </summary>
public class CameraHandler : MonoBehaviour
{
    /// <summary>
    /// Target which the camera has to follow
    /// </summary>
    [SerializeField]
    [Tooltip("Add target to smoothly follow")]
    private Transform target;

    /// <summary>
    /// Sets speed of smoothening of the camera movement
    /// </summary>
    [SerializeField]
    [Tooltip("Set value for smooth camera follow between 0 and 1")]
    [Range(0f, 1f)]
    private float smoothSpeed = 0.5f;

    /// <summary>
    /// Offset of the camera in relation to the targeted object
    /// </summary>
    [SerializeField]
    [Tooltip("Set camera offset to the followed object")]
    private Vector3 positionOffset;

    // For some reason velocity?
    private Vector3 velocity = Vector3.zero;

    /// <summary>
    /// Update for the camera
    /// </summary>
    // Update but... late?
    void LateUpdate()
    {
        Vector3 desiredPosition = target.transform.position + positionOffset;

        Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);

        transform.position = smoothedPosition;

        transform.LookAt(target.transform.position);
    }
}

