﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// The airplane script manages all the control inputs and passes them on to all the registered control surfaces.
/// It can also modify the thrust of the engine script
/// </summary>
public class Airplane : MonoBehaviour
{

/// <summary>
/// This is a basic control surface setting pretty much applicable for any aircraft. 
/// It is possible to have aircrafts with multiple rudders or elevators, in this case add more constrolsurfaces.
/// </summary>
	public ControlSurface elevator;
	public ControlSurface aileronLeft;
	public ControlSurface aileronRight;
	public ControlSurface rudder;

	/// <summary>
	/// Engine used to change the speed of the airplane
	/// </summary>
	public Thrust engine;

	/// <summary>
	/// Standard RigidBody
	/// </summary>
	public Rigidbody Rigidbody { get; internal set; }

	/// <summary>
	/// Standard throttle set at 100%
	/// </summary>
	private float throttle = 1.0f;
	private bool yawDefined = false;

	private void Awake()
	{
		Rigidbody = GetComponent<Rigidbody>();
	}

	/// <summary>
	/// Is mostly used to error check
	/// </summary>
	private void Start()
	{
		if (elevator == null)
			Debug.LogWarning(name + ": Airplane missing elevator!");
		if (aileronLeft == null)
			Debug.LogWarning(name + ": Airplane missing left aileron!");
		if (aileronRight == null)
			Debug.LogWarning(name + ": Airplane missing right aileron!");
		if (rudder == null)
			Debug.LogWarning(name + ": Airplane missing rudder!");
		if (engine == null)
			Debug.LogWarning(name + ": Airplane missing engine!");

		try
		{
			Input.GetAxis("Yaw");
			yawDefined = true;
		}
		catch (ArgumentException e)
		{
			Debug.LogWarning(e);
			Debug.LogWarning(name + ": \"Yaw\" axis not defined in Input Manager. Rudder will not work correctly!");
		}
	}

	/// <summary>
	/// Called everyframe and listens to inputs detected by the unity input manager.
	/// Then passes these inputs on as deflections to the controlsurfaces.
	/// </summary>
	void Update()
	{
		if (elevator != null)
		{
			elevator.targetDeflection = -Input.GetAxis("Vertical");
		}
		if (aileronLeft != null)
		{
			aileronLeft.targetDeflection = -Input.GetAxis("Horizontal");
		}
		if (aileronRight != null)
		{
			aileronRight.targetDeflection = Input.GetAxis("Horizontal");
		}
		if (rudder != null && yawDefined)
		{
			//There must a be new axis defined for yaw to work
			//rudder.targetDeflection = Input.GetAxis("Yaw");
		}

		if (engine != null)
		{
			// lmb to speed up, rmb to slow down. Make sure throttle only goes 0-1.
			throttle += Input.GetAxis("Fire1") * Time.deltaTime;
			throttle -= Input.GetAxis("Fire2") * Time.deltaTime;
			throttle = Mathf.Clamp01(throttle);

			engine.throttle = throttle;
		}
	}
}
