﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic script that handles the thrust and forward movement of any gameobject.
/// </summary>
public class Thrust : MonoBehaviour
{

/// <summary>
/// Throttle that can range from 0 to 1.
/// </summary>
	[Range(0, 1)]
	public float throttle = 1.0f;

	/// <summary>
	/// Thrust to dictate how much power is applied to the forward movement.
	/// </summary>
	[Tooltip("How much power the engine puts out.")]
	public float thrust;

	private Rigidbody rigid;

	private void Awake()
	{
		rigid = GetComponentInParent<Rigidbody>();
	}

	/// <summary>
	/// Simlply uses the Unity AddRelativeForce method to apply the thurst force.
	/// </summary>
	private void FixedUpdate()
	{
		if (rigid != null)
		{
			rigid.AddRelativeForce(Vector3.forward * thrust * throttle, ForceMode.Force);
		}
	}
}
