﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Aerofoil curves script generates and keeps certain coefficient values saved which are needed for the physics calculations based on which aerofoil design is configured.
/// Currently only the drag and lift coefficient values proportionate to the angle of attack are saved. 
/// These can be accessed during physics calculation based on the given Angle of Attack.
/// The generated coefficients be viewed with the help of the animationCurves that are found in the Aerofoil Inspector window.
/// The configuration of these curves is done in the AerofoilEditor script.
/// </summary>
[CreateAssetMenu(fileName = "New Aerofoil", menuName = "Aerofoil")]
public class Aerofoil : ScriptableObject
{

    /// <summary>
    /// Basic description of the airfoil
    /// </summary>
    [TextArea]
    public string description;


    /// <summary>
    /// This curve represents the Lift Coefficient values at different angles of attack, this curve is of extreme importance since these values have the most affect on the handling of the plane.
    /// </summary>
    [SerializeField]
    [Tooltip("This curve represents the Lift Coefficient values at different angles of attack, this curve is of extreme importance since these values have the most affect on the handling of the plane")]
    private AnimationCurve lift = new AnimationCurve();

    /// <summary>
    /// This curve represents the drag coefficient values at different angles of attack, this curve is also very important, but due to not conclusive research this cannot be modified (yet!).
    /// </summary>
    [SerializeField]
    [Tooltip("This curve represents the drag coefficient values at different angles of attack, this curve is also very important, but due to not conclusive research this cannot be modified (yet!)")]

    /// <summary>
    /// Basic drag curve for simplicty sake.
    /// </summary>
    private AnimationCurve drag = new AnimationCurve(
        new Keyframe(0.0f, 0.025f),
        new Keyframe(90f, 1f),
        new Keyframe(180f, 0.025f));

    /// <summary>
    /// Returns the specific LiftCoefficient at the given angle of attack.
    /// </summary>
    /// <param name="aoa"></param>
    /// <returns>float LiftCoefficient</returns>
    public float GetLiftAtAngleOfAttack(float aoa) {
        return lift.Evaluate(aoa);
    }

    /// <summary>
    /// Returns the specific DragCoefficient at the given angle of attack
    /// </summary>
    /// <param name="aoa"></param>
    /// <returns>float DragCoefficient</returns>
    public float GetDragAtAngleOfAttack(float aoa) {
        return drag.Evaluate(aoa);
    }
    
    /// <summary>
    /// Generate a new wing curve based on the configuration
    /// </summary>
    /// <param name="newCurve"></param>
    public void SetLiftCurve(Keyframe[] newCurve) {
        lift.keys = newCurve;
    }

}
