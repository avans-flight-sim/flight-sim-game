﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Control surface script handles the logic behind the control surfaces.
/// This script adds more functionality to the wing script turning it into usable control surfaces.
/// </summary>
public class ControlSurface : MonoBehaviour
{
	
	/// <summary>
	/// Set the max deflection
	/// </summary>
	[Header("Deflection")]
	[Tooltip("Deflection with max positive input."), Range(0, 90)]
	public float max = 15f;

	/// <summary>
	/// Set the min deflection
	/// </summary>
	[Tooltip("Deflection with max negative input"), Range(0, 90)]
	public float min = 15f;

	/// <summary>
	/// Speed at which the control surface moves
	/// </summary>
	[Tooltip("Speed of the control surface deflection.")]
	public float moveSpeed = 90f;

	/// <summary>
	/// TargetDelection which is the standard angle at which the deflection is set to
	/// </summary>
	[Tooltip("Requested deflection of the control surface normalized to [-1, 1]. "), Range(-1, 1)]
	public float targetDeflection = 0f;

	/// <summary>
	/// The wing which provides the various aerodynamical properties of a control surface
	/// </summary>
	[Header("Speed Stiffening")]

	[Tooltip("Wing to use for deflection forces. Deflection limited based on " +
		"airspeed will not function without a reference wing.")]
	[SerializeField] private Wing wing = null;

	/// <summary>
	/// How much force the control surface can exert. The lower this is, the more the control surface stiffens with speed.
	/// </summary>
	[Tooltip("How much force the control surface can exert. The lower this is, " +
		"the more the control surface stiffens with speed.")]
	public float maxTorque = 6000f;

	private Rigidbody rigid = null;
	private Quaternion startLocalRotation = Quaternion.identity;

	private float angle = 0f;

	private void Awake()
	{
		// If the wing has been referenced, then control stiffening will want to be used.
		if (wing != null)
			rigid = GetComponentInParent<Rigidbody>();
	}

	private void Start()
	{
		// Dirty hack so that the rotation can be reset before applying the deflection.
		startLocalRotation = transform.localRotation;
	}

	private void FixedUpdate()
	{
		// Different angles depending on positive or negative deflection.
		float targetAngle = targetDeflection > 0f ? targetDeflection * max : targetDeflection * min;

		// How much you can deflect, depends on how much force it would take
		if (rigid != null && wing != null && rigid.velocity.sqrMagnitude > 1f)
		{
			float torqueAtMaxDeflection = rigid.velocity.sqrMagnitude * wing.WingArea;
			float maxAvailableDeflection = Mathf.Asin(maxTorque / torqueAtMaxDeflection) * Mathf.Rad2Deg;

			// Asin(x) where x > 1 or x < -1 is not a number.
			if (float.IsNaN(maxAvailableDeflection) == false)
				targetAngle *= Mathf.Clamp01(maxAvailableDeflection);
		}

		// Move the control surface.
		angle = Mathf.MoveTowards(angle, targetAngle, moveSpeed * Time.fixedDeltaTime);

		// Hacky way to do this!
		transform.localRotation = startLocalRotation;
		transform.Rotate(Vector3.right, angle, Space.Self);
	}
}
