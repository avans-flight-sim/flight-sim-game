﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The wing script stores and handles all the physics behaviour of a single wing. 
/// This script has to be attached to an empty game object and can be configured into a proper wing.
/// Afterwards this scripts needs to have a proper aerofoil script attached to it in order to access the proper lift and drag coefficients assosiated witht the wing.
/// </summary>
public class Wing : MonoBehaviour
{
/// <summary>
/// Dimension of the wing defined in an x and y value
/// </summary>
	[Tooltip("Size of the wing. The bigger the wing, the more lift it provides.")]
	public Vector2 dimensions = new Vector2(5f, 2f);

	/// <summary>
	/// Option to apply the calculated forces to the main COM of the airplane, instead of the centre of the airfoil itself
	/// </summary>
	[Tooltip("When true, wing forces will be applied only at the center of mass.")]
	public bool applyForcesToCenter = false;

	/// <summary>
	/// Aerofoil script containing the various coefficients data used for the physics calculations
	/// </summary>
	[Tooltip("Lift coefficient curve.")]
	public Aerofoil wing;

	/// <summary>
	/// Multiplier for lift strenght
	/// </summary>
	[Tooltip("The higher the value, the more lift the wing applie at a given angle of attack.")]
	public float liftMultiplier = 1f;

	/// <summary>
	/// Multiplier for lift strenght
	/// </summary>
	[Tooltip("The higher the value, the more drag the wing incurs at a given angle of attack.")]
	public float dragMultiplier = 1f;

	private Rigidbody rigid;

	/// <summary>
	/// Specify the direction of the resulting lift force
	/// </summary>
	private Vector3 liftDirection = Vector3.up;

	/// <summary>
	/// Temporary value
	/// </summary>
	private float liftCoefficient = 0f;
	/// <summary>
	/// Temporary value
	/// </summary>
	private float dragCoefficient = 0f;
	/// <summary>
	/// Temporary value
	/// </summary>
	private float liftForce = 0f;
	/// <summary>
	/// Temporary value
	/// </summary>
	private float dragForce = 0f;

	private float angleOfAttack = 0f;

	/// <summary>
	/// Value that was used in the original code to fix a issue in the physics substep
	/// </summary>
	const float PREDICTION_TIMESTEP_FRACTION = 0.5f;

	/// <summary>
	/// Retrieves the angle of attack of the wing
	/// </summary>
	public float AngleOfAttack
	{
		get
		{
			if (rigid != null)
			{
				Vector3 localVelocity = transform.InverseTransformDirection(rigid.velocity);
				return angleOfAttack * -Mathf.Sign(localVelocity.y);
			}
			else
			{
				return 0.0f;
			}
		}
	}

	/// <summary>
	/// Calculates WingArea
	/// </summary>
	public float WingArea
	{
		get { return dimensions.x * dimensions.y; }
	}

	public float LiftCoefficient { get { return liftCoefficient; } }
	public float LiftForce { get { return liftForce; } }
	public float DragCoefficient { get { return dragCoefficient; } }
	public float DragForce { get { return dragForce; } }

	public Rigidbody Rigidbody
	{
		set { rigid = value; }
	}

	private void Awake()
	{
		rigid = GetComponentInParent<Rigidbody>();
	}

	private void Start()
	{
		if (rigid == null)
		{
			Debug.LogError(name + ": SimpleWing has no rigidbody on self or parent!");
		}

		if (wing == null)
		{
			Debug.LogError(name + ": SimpleWing has no defined wing curves!");
		}
	}

	/// <summary>
	/// Since we're not dealing with physics the Update method only contains some error prevention and debugging.
	/// </summary>
	private void Update()
	{
		// Prevent division by zero.
		if (dimensions.x <= 0f)
			dimensions.x = 0.01f;
		if (dimensions.y <= 0f)
			dimensions.y = 0.01f;

		// DEBUG
		if (rigid != null)
		{
			Debug.DrawRay(transform.position, liftDirection.normalized * liftForce * 0.0001f, Color.blue);
			Debug.DrawRay(transform.position, -rigid.velocity.normalized * dragForce * 0.0001f, Color.red);
		}
	}

	/// <summary>
	/// FixedUpdate contains all the necasarry physics calculations that are ran every physics substep.
	/// Original code can be found below which is disabled due to instability
	/// </summary>
	private void FixedUpdate()
	{
		if (rigid != null && wing != null)
		{
			Vector3 forceApplyPos = (applyForcesToCenter) ? rigid.transform.TransformPoint(rigid.centerOfMass) : transform.position;

			Vector3 localVelocity = transform.InverseTransformDirection(rigid.GetPointVelocity(transform.position));
			localVelocity.x = 0f;

			// Angle of attack is used as the look up for the lift and drag curves.
			angleOfAttack = Vector3.Angle(Vector3.forward, localVelocity);
			liftCoefficient = wing.GetLiftAtAngleOfAttack(angleOfAttack);
			dragCoefficient = wing.GetDragAtAngleOfAttack(angleOfAttack);

			// Calculate lift/drag. based on lift and drag formulas
			liftForce = localVelocity.sqrMagnitude * liftCoefficient * WingArea * liftMultiplier;
			dragForce = localVelocity.sqrMagnitude * dragCoefficient * WingArea * dragMultiplier;

			// Vector3.Angle always returns a positive value, so add the sign back in.
			liftForce *= -Mathf.Sign(localVelocity.y);

			//if (liftForce > 0)
			//{
			//	Debug.Log("Higher than 0" + localVelocity + this.name);
			//}

			//if (liftForce < 0)
			//{
			//	Debug.Log("Lower than 0" + localVelocity + this.name);
			//}

			// Lift is always perpendicular to air flow.
			liftDirection = Vector3.Cross(rigid.velocity, transform.right).normalized;
			rigid.AddForceAtPosition(liftDirection * liftForce, forceApplyPos, ForceMode.Force);

			// Drag is always opposite of the velocity.
			rigid.AddForceAtPosition(-rigid.velocity.normalized * dragForce, forceApplyPos, ForceMode.Force);
		}
	}

	// Prevent this code from throwing errors in a built game.
#if UNITY_EDITOR
/// <summary>
/// This method helps visualising the wings in the editor.
/// </summary>
	private void OnDrawGizmosSelected()
	{
		Matrix4x4 oldMatrix = Gizmos.matrix;

		Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
		Gizmos.color = Color.white;
		Gizmos.DrawCube(Vector3.zero, new Vector3(dimensions.x, 0f, dimensions.y));

		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(Vector3.zero, new Vector3(dimensions.x, 0f, dimensions.y));

		Gizmos.matrix = oldMatrix;
	}
#endif

	//    [Tooltip("Wing area of the wing, first variable is the width, second the height. The bigger the wing, the more lift it provides.")]
	//    public Vector2 wingArea = new Vector2(8f, 3f);

	//    [Tooltip("The aerofoil design used by the wing. This determines the lift and drag characteristics of the wing.")]
	//    public Aerofoil aerofoil;

	//    private Rigidbody rigidbody;

	//    //The force that handle the lift and therefor flying of the airplane. This force will be modified depending on the calculations ran. The direction of this force is up (duh).
	//    private Vector3 liftForceDirection = Vector3.up;
	//    private float AoA = 0f;

	//    private float liftCoefficient = 0f;
	//    private float dragCoefficient = 0f;
	//    private float liftForce = 0f;
	//    private float dragForce = 0f;

	//    private Vector3 liftForceVector;
	//    private Vector3 dragForceVector;

	//    private Vector3 VelocityPrediction;

	//    private Vector3 liftForceVectorPrediction;
	//    private Vector3 dragForceVectorPrediction;

	//    private Vector3 currentLiftForceVector;
	//    private Vector3 currentDragForceVector;

	//    public float LiftCoefficient { get { return liftCoefficient; } }
	//    public float LiftForce { get { return liftForce; } }
	//    public float DragCoefficient { get { return dragCoefficient; } }
	//    public float DragForce { get { return dragForce; } }

	//    const float PREDICTION_TIMESTEP_FRACTION = 0.5f;

	//    public Rigidbody Rigidbody
	//    {
	//        set { rigidbody = value; }
	//    }

	//    public float AngleOfAttack
	//    {
	//        get
	//        {
	//            if (rigidbody != null)
	//            {
	//                Vector3 localVelocity = transform.InverseTransformDirection(rigidbody.velocity);
	//                return AoA * -Mathf.Sign(localVelocity.y);
	//            }
	//            else
	//            {
	//                return 0.0f;
	//            }
	//        }
	//    }

	//    private void Awake()
	//    {
	//        rigidbody = GetComponentInParent<Rigidbody>();
	//    }

	//    // Start is called before the first frame update.
	//    void Start()
	//    {
	//        if (rigidbody == null || aerofoil == null) {
	//            Debug.LogError(name + ": Either rigidbother or the aerofoil parameters are null. Make sure these are set!");
	//        }
	//    }

	//    // Update is called once per frame will be mostly used for debugging.
	//    void Update()
	//    {
	//        //DRAW FORCE LINES
	//            //Multiply by 0.0001 otherwise these are waaaaaay too long.
	//            Debug.DrawRay(transform.position, liftForceDirection.normalized * liftForce * 0.0001f, Color.blue);
	//            Debug.DrawRay(transform.position, -rigidbody.velocity.normalized * dragForce * 0.0001f, Color.red);
	//    }

	//    private void OnDrawGizmosSelected()
	//    {
	//#if UNITY_EDITOR
	//        Matrix4x4 oldMatrix = Gizmos.matrix;

	//        Gizmos.color = Color.white;

	//        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
	//        Gizmos.DrawCube(Vector3.zero, new Vector3(wingArea.x, 0f, wingArea.y));

	//        Gizmos.color = Color.red;
	//        Gizmos.DrawWireCube(Vector3.zero, new Vector3(wingArea.x, 0f, wingArea.y));

	//        Gizmos.matrix = oldMatrix;
	//#endif
	//    }

	//    private void angleOfAttack(Vector3 localV){
	//        AoA = Vector3.Angle(Vector3.forward, localV);
	//        liftCoefficient = aerofoil.GetLiftAtAngleOfAttack(AoA);
	//        dragCoefficient = aerofoil.GetDragAtAngleOfAttack(AoA);
	//    }

	//    private void CalculateForces() {
	//        Vector3 localVelocity = transform.InverseTransformDirection(rigidbody.GetPointVelocity(transform.position));
	//        localVelocity.x = 0f;

	//        angleOfAttack(localVelocity);

	//        liftForce = localVelocity.sqrMagnitude * liftCoefficient * (wingArea.x * wingArea.y);

	//        dragForce = localVelocity.sqrMagnitude * dragCoefficient * (wingArea.x * wingArea.y);

	//        liftForce *= -Mathf.Sign(localVelocity.y);

	//        liftForceDirection = Vector3.Cross(rigidbody.velocity, transform.right).normalized;

	//        liftForceVector = liftForceDirection * liftForce;
	//        dragForceVector = -rigidbody.velocity.normalized * dragForce;
	//    }

	//    private void PredictForces() {
	//        Vector3 localVelocity = transform.InverseTransformDirection(rigidbody.GetPointVelocity(transform.position)) * (Time.fixedDeltaTime * PREDICTION_TIMESTEP_FRACTION);
	//        localVelocity.x = 0f;

	//        angleOfAttack(localVelocity);

	//        liftForce = localVelocity.sqrMagnitude * liftCoefficient * (wingArea.x * wingArea.y);

	//        dragForce = localVelocity.sqrMagnitude * dragCoefficient * (wingArea.x * wingArea.y);

	//        liftForce *= -Mathf.Sign(localVelocity.y * (Time.fixedDeltaTime * PREDICTION_TIMESTEP_FRACTION));

	//        liftForceDirection = Vector3.Cross(rigidbody.velocity * (Time.fixedDeltaTime * PREDICTION_TIMESTEP_FRACTION), transform.right).normalized;

	//        liftForceVectorPrediction = liftForceDirection * liftForce;
	//        dragForceVectorPrediction = -rigidbody.velocity.normalized * dragForce;
	//    }

	//    // Fixed update is called one per physics frame all the calculations will be here.
	//    private void FixedUpdate()
	//    {
	//        //if(rigidbody != null && aerofoil != null) {

	//        //    CalculateForces();
	//        //    PredictForces();

	//        //    currentLiftForceVector = (liftForceVector + liftForceVectorPrediction) * 0.5f;
	//        //    currentDragForceVector = (dragForceVector + dragForceVectorPrediction) * 0.5f;

	//        //    rigidbody.AddForceAtPosition(currentDragForceVector, transform.position, ForceMode.Force);
	//        //    rigidbody.AddForceAtPosition(currentDragForceVector, transform.position, ForceMode.Force);
	//        //}

	//        if (rigidbody != null && aerofoil != null)
	//        {
	//            Vector3 forceApplyPos = rigidbody.transform.TransformPoint(rigidbody.centerOfMass);

	//            Vector3 localVelocity = transform.InverseTransformDirection(rigidbody.GetPointVelocity(transform.position));
	//            localVelocity.x = 0f;

	//            // Angle of attack is used as the look up for the lift and drag curves.
	//            AoA = Vector3.Angle(Vector3.forward, localVelocity);
	//            liftCoefficient = aerofoil.GetLiftAtAngleOfAttack(AoA);
	//            dragCoefficient = aerofoil.GetDragAtAngleOfAttack(AoA);

	//            // Calculate lift/drag.
	//            liftForce = localVelocity.sqrMagnitude * liftCoefficient * (wingArea.x * wingArea.y);
	//            dragForce = localVelocity.sqrMagnitude * dragCoefficient * (wingArea.x * wingArea.y);

	//            // Vector3.Angle always returns a positive value, so add the sign back in.
	//            liftForce *= -Mathf.Sign(localVelocity.y);

	//            // Lift is always perpendicular to air flow.
	//            liftForceDirection = Vector3.Cross(rigidbody.velocity, transform.right).normalized;

	//            if(liftForceDirection.z < 0 || liftForceDirection.y < 0) {
	//                Debug.LogError("Error with liftforce direction");
	//            }

	//            if (liftForce < 0)
	//            {
	//                Debug.LogError("Error with liftforce direction");
	//            }

	//            rigidbody.AddForceAtPosition(liftForceDirection * liftForce, forceApplyPos, ForceMode.Force);

	//            // Drag is always opposite of the velocity.
	//            rigidbody.AddForceAtPosition(-rigidbody.velocity.normalized * dragForce, forceApplyPos, ForceMode.Force);
	//        }
	//    }
}
