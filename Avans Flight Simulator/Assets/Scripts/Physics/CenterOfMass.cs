﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is a usefull adition in order to alter the center of mass of certain gameobject.
/// This adds a slight level of realism and makes the plane handle a lot better.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class CenterOfMass : MonoBehaviour
{

/// <summary>
/// Vector for COM.
/// </summary>
    public Vector3 CenterOfMass2;
    public bool Awake;
    protected Rigidbody r;

    /// <summary>
    /// Simply overrides the old COM with the new COM.
    /// </summary>
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Rigidbody>();

        //Set COM 
        r.centerOfMass = CenterOfMass2;
        r.WakeUp();
        Awake = !r.IsSleeping();
    }

    /// <summary>
    /// Some gizmos for debugging
    /// </summary>
    private void OnDrawGizmosSelected()
    {
    //Some debug gizmos
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + transform.rotation * CenterOfMass2, 1f);
    }
}
