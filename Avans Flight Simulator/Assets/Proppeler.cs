﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proppeler : MonoBehaviour
{
    public Thrust Engine;

    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frame
    void Update()
    {
        float force = 100 * Engine.throttle;
        if(force < 50) {
            force = 50;
        }
        transform.Rotate(transform.rotation.x, (transform.rotation.y + force ), transform.rotation.z);
    }
}
